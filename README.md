# Symfony-starter-pack

## Requirements 

Note: this starter pack is written for linux, adapt accordingly

Before creating your first Symfony application (version 6.x) you must:

* Install PHP 8.0.2 or higher and these PHP extensions (which are installed and enabled by default in most PHP 8 installations): Ctype, iconv, PCRE, Session, SimpleXML, and Tokenizer;

* Install Composer, which is used to install PHP packages.

## Installation

#### Install Symfony CLI 

In case of starting a project by Symfony CLI

```
echo 'deb [trusted=yes] https://repo.symfony.com/apt/ /' | sudo tee /etc/apt/sources.list.d/symfony-cli.list
sudo apt update
sudo apt install symfony-cli
```

#### To check if symfony is running well
```
symfony check:requirements
```

## To create project 

#### Create with Composer

Run this if you are building a traditional web application
```
composer create-project symfony/skeleton my_project_directory
cd my_project_directory
composer require webapp
```

Run this if you are building a microservice, console application or API
```
* composer create-project symfony/skeleton my_project_directory
```


#### Create with Symfony CLI
```
# Run this if you are building a traditional web application
    symfony new my_project_directory --webapp

# Run this if you are building a microservice, console application or API
    symfony new my_project_directory
```
## To Run Project

1. Go in directory `cd my-project/`
2. Open server with `symfony server:start`

## Database Connection

#### Install doctrine support
Doctrine is a set of php library to manage databases. These tools support relational databases like MySQL and PostgreSQL and also NoSQL databases like MongoDB.

To install run the following commands:

1. `composer require symfony/orm-pack`
2. `composer require --dev symfony/maker-bundle`

3. In your `.env` file (or override DATABASE_URL in .env.local to avoid committing your changes), uncomment the chosen database url and add in your information

```
# customize this line!
DATABASE_URL="mysql://db_user:db_password@127.0.0.1:3306/db_name?serverVersion=5.7"

# to use mariadb:
DATABASE_URL="mysql://db_user:db_password@127.0.0.1:3306/db_name?serverVersion=mariadb-10.5.8"

# to use sqlite:
DATABASE_URL="sqlite:///%kernel.project_dir%/var/app.db"

# to use postgresql:
DATABASE_URL="postgresql://db_user:db_password@127.0.0.1:5432/db_name?serverVersion=11&charset=utf8"

# to use oracle:
DATABASE_URL="oci8://db_user:db_password@127.0.0.1:1521/db_name"
```

4. to create a database once your connection is configured run 
```
php bin/console doctrine:database:create
```

## Database migration and entity

1. to create an entity run 
```
php bin/console make:entity
``` 
follow instructions to add properties
2. to migrate run 
```
php bin/console make:migration
```
3. to add to database run 
```
php bin/console doctrine:migrations:migrate
```
